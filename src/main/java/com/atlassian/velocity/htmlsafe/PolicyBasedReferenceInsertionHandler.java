package com.atlassian.velocity.htmlsafe;

import org.apache.velocity.app.event.ReferenceInsertionEventHandler;
import org.apache.velocity.context.Context;
import org.apache.velocity.util.ContextAware;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A {@link ReferenceInsertionEventHandler} that delegates reference insertion processing to the provided
 * {@link ReferenceInsertionPolicy}.
 */
public final class PolicyBasedReferenceInsertionHandler implements ReferenceInsertionEventHandler, ContextAware {
    private Context context;

    private final ReferenceInsertionPolicy insertionPolicy;

    public PolicyBasedReferenceInsertionHandler(ReferenceInsertionPolicy insertionPolicy) {
        this.insertionPolicy = checkNotNull(insertionPolicy, "insertionPolicy must not be null");
    }

    public Object referenceInsert(String reference, Object value) {
        return insertionPolicy.getReferenceInsertionEventHandler(context).referenceInsert(reference, value);
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
