package com.atlassian.velocity.htmlsafe.context;

import org.apache.velocity.app.event.EventCartridge;

/**
 * General interface for classes that process Velocity event cartridges in some way.
 */
public interface EventCartridgeProcessor {
    /**
     * Process the provided context in accordance with this processing strategy.
     *
     * @param cartridge Cartridge to process
     */
    void processCartridge(EventCartridge cartridge);
}
