package com.atlassian.velocity.htmlsafe.directive;

import org.apache.velocity.Template;

/**
 * Checks whether a directive is present by traversing a Template's AST.
 *
 * @since v1.1.1
 */
public class DefaultDirectiveChecker implements DirectiveChecker {
    /**
     * @see Directives#isPresent(String, org.apache.velocity.Template)
     */
    @Override
    public boolean isPresent(String directiveName, Template template) {
        return Directives.isPresent(directiveName, template);
    }
}
