package com.atlassian.velocity.htmlsafe.introspection;

/**
 * The value represents a boxed value. (i.e. any value that has been wrapped but should still be accessible)
 */
public interface BoxedValue<T> {
    T unbox();
}
