package com.atlassian.velocity.htmlsafe.introspection;

import java.lang.annotation.Annotation;
import java.util.Collection;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * {@link AnnotationBoxedElement} that delegates all operations to
 * the wrapped element except for toString() which is delegated to the boxed value itself.
 */
public final class ToStringDelegatingAnnotationBoxedElement<E> implements AnnotationBoxedElement<E> {
    private final AnnotationBoxedElement<E> delegate;

    public ToStringDelegatingAnnotationBoxedElement(AnnotationBoxedElement<E> delegate) {
        this.delegate = checkNotNull(delegate, "delegate must not be null");
    }

    public E unbox() {
        return delegate.unbox();
    }

    public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
        return delegate.isAnnotationPresent(annotationType);
    }

    public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
        return delegate.getAnnotation(annotationType);
    }

    public Annotation[] getAnnotations() {
        return delegate.getAnnotations();
    }

    public Annotation[] getDeclaredAnnotations() {
        return delegate.getDeclaredAnnotations();
    }

    public Collection<Annotation> getAnnotationCollection() {
        return delegate.getAnnotationCollection();
    }

    public <T extends Annotation> boolean hasAnnotation(final Class<T> clazz) {
        return delegate.hasAnnotation(clazz);
    }

    public Object box(Object value) {
        final Object boxedValue = delegate.box(value);
        if (boxedValue instanceof AnnotationBoxedElement) {
            return new ToStringDelegatingAnnotationBoxedElement((AnnotationBoxedElement<?>) boxedValue);
        }
        return boxedValue;
    }

    public String toString() {
        return String.valueOf(delegate.unbox());
    }
}
