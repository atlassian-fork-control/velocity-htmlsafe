package com.atlassian.velocity.htmlsafe;

import junit.framework.TestCase;

/**
 * Tests for HtmlAnnotationUtils
 */
public class TestHtmlAnnotationUtils extends TestCase {
    public void testCanonicalHtmlSafeAnnotation() {
        assertNotNull(HtmlSafeAnnotationUtils.HTML_SAFE_ANNOTATION);
        assertTrue(HtmlSafeAnnotationUtils.HTML_SAFE_ANNOTATION.annotationType().equals(HtmlSafe.class));
    }
}
