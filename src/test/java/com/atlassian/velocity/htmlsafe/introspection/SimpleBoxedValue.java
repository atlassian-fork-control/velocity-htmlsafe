package com.atlassian.velocity.htmlsafe.introspection;

/**
 * Primitive boxed value wrapper
 */
final class SimpleBoxedValue implements BoxedValue {
    private final Object boxedObject;

    public SimpleBoxedValue(Object boxedObject) {
        this.boxedObject = boxedObject;
    }

    public Object unbox() {
        return boxedObject;
    }
}
