package com.atlassian.velocity.htmlsafe.introspection;

import junit.framework.TestCase;

import static org.mockito.Mockito.mock;

/**
 * Tests for the AnnotatedValueStringHandler
 */
public class TestAnnotatedValueStringHandler extends TestCase {
    public void testHandlerReturnsDelegatorForAnnotatedValue() {
        AnnotationBoxedElement mockAnnotatedValue = mock(AnnotationBoxedElement.class);

        AnnotatedValueStringHandler stringHandler = new AnnotatedValueStringHandler();
        Object result = stringHandler.referenceInsert("foo", mockAnnotatedValue);
        assertNotNull(result);
        assertTrue(result instanceof ToStringDelegatingAnnotationBoxedElement);
    }

    public void testHandlerReturnsOriginalValueWhenNotAnnotated() {
        Object value = new Object();
        AnnotatedValueStringHandler stringHandler = new AnnotatedValueStringHandler();
        Object result = stringHandler.referenceInsert("foo", value);
        assertNotNull(result);
        assertEquals(value, result);
    }

    public void testHandlerReturnsOriginalValueWhenNull() {
        AnnotatedValueStringHandler stringHandler = new AnnotatedValueStringHandler();
        Object result = stringHandler.referenceInsert("foo", null);
        assertNull(result);
    }
}
